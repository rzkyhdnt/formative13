package formative2;
import java.time.LocalDate;
import java.util.ArrayList;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
  
@Controller
public class GreetController {
  
    @RequestMapping("/greet")
    public ModelAndView showview() {
    	LocalDate time = LocalDate.now();
    	ArrayList<String> listName = new ArrayList<String>();
    	ArrayList<String> lastName = new ArrayList<String>();
    	ArrayList<String> email = new ArrayList<String>();
    	ArrayList<String> temp = new ArrayList<String>();
    	listName.add("Bambang");
    	listName.add("Suci");
    	listName.add("Budi");
    	listName.add("Septi");
    	listName.add("Deni");
    	listName.add("Dedi");
    	listName.add("Dodi");
    	listName.add("Selo");
    	listName.add("Mahfud");
    	listName.add("Gerard");
    	
    	lastName.add("Sopanji");
    	lastName.add("Feri");
    	lastName.add("Sopanji");
    	lastName.add("Burhan");
    	lastName.add("Malih");
    	lastName.add("Sopian");
    	lastName.add("Heriyadi");
    	lastName.add("Yodi");
    	lastName.add("Hadinata");
    	lastName.add("Kalifa");
    	
    	email.add("bambangsopanji@gmail.com");
    	email.add("suciferi@gmail.com");
    	email.add("budisopanji@gmail.com");
    	email.add("septiburhan@gmail.com");
    	email.add("denimalih@gmail.com");
    	email.add("dedisopian@gmail.com");
    	email.add("dodiheriyadi@gmail.com");
    	email.add("seloyodi@gmail.com");
    	email.add("mahfudhadinata@gmail.com");
    	email.add("gerardkalifa@gmail.com");
    	
    	for(int i=0; i<listName.size()-1; i++) {
    		temp.add(listName.get(i) + " " + lastName.get(i) + " " + email.get(i) + "\n");
    	}
    	
        ModelAndView mv = new ModelAndView();
        mv.setViewName("Result.jsp");
        mv.addObject("result", temp);
        return mv;
    }
}