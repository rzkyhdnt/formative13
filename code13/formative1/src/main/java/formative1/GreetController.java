package formative1;
import java.time.LocalDate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
  
@Controller
public class GreetController {
  
    @RequestMapping("/greet")
    public ModelAndView showview() {
    	LocalDate time = LocalDate.now();
        ModelAndView mv = new ModelAndView();
        mv.setViewName("Result.jsp");
        mv.addObject("result", "Hallo " + time);
        return mv;
    }
}